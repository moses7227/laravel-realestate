<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\welcomecontroller;
use App\Http\Controllers\regiscontroller;
use App\Http\Controllers\StudInsertController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('/moses',[welcomecontroller::class,'index'])->name('moses.index');*/


/*Route::get('/home', function () {
    return view('index');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/content', function () {
    return view('content');
});*/
/*
Route::get('registration',[regiscontroller::class,'index']);
Route::post('registration',[regiscontroller::class,'add']);*/

/*
Route::get('registration', function($add)
{
    return View::make('pages.add');
});
*/
Route::get('home', function()
{
    return View::make('pages.home');
});
Route::get('about', function()
{
    return View::make('pages.about');
});
Route::get('projects', function()
{
    return View::make('pages.projects');
});
Route::get('contact', function()
{
    return View::make('pages.contact');
});
Route::get('blogs', function()
{
    return View::make('pages.blog');
});
/*
Route::get('insert','StudInsertController@insertform');
Route::post('create','StudInsertController@insert');
*/

Route::get('registration','regiscontroller@index');
Route::post('registration','regiscontroller@add');


